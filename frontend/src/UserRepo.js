import React from 'react'
import './App.css'

const UserRepo = props => {
    props.repo.owner.avatar_url = 'http://localhost:5000/static/images/oauth.jpg'
    
    return (
      <div>
        <li className='contact-list-item'>
          <div className='contact-avatar'
            style={{
              backgroundImage: `url(${props.repo.owner.avatar_url})`
            }}
          ></div>
          <div className='contact-details'>
            <p><b>{props.repo.name}</b></p>
            <p>{props.repo.description}</p>
          </div>
        </li>
      </div>
    )
}

export default UserRepo;