import React, { Component } from 'react'
import logo from './logo.svg';
import * as ContactsAPI from './utils/ContactsAPI'
// import Cookies from 'js-cookie';
import Login from './Login';
import UserProfile from './UserProfile'
import './App.css';

const api = 'http://localhost:5000'

class App extends Component {
  state = {
    isAuthenticated: 'false',
  }
  componentDidMount() {
    ContactsAPI.getUserAuth()
      .then((auth) => {
        this.setState(() => ({
          isAuthenticated: auth
        }))
      })
  };
  fetchLogoutAPI = () => {
    fetch(`${api}/?action=logout`, {
      mode: 'cors',
      credentials: 'include',
    })
    .then(res => res.json())
    .then((data) => {
      this.setState(() => ({
        isAuthenticated: data["is_authenticated"],
      }))
    })
  };

  render() {
    const viewLogOutButton = (
      <div>
        <button onClick={this.fetchLogoutAPI}>
          {this.state.isAuthenticated ? 'Logout' : ''}
        </button>
      </div>
    );

    const UserPage = (
      <div>
        <UserProfile/>
        {this.state.isAuthenticated ? (viewLogOutButton) : ('')}
      </div>      
    );

    const renderHomePage = (
      <div>
        {this.state.isAuthenticated === "false" ? (<Login/>) : (UserPage)}
      </div>
      );

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">My GitHub Repos</h1>
        </header>
        {renderHomePage}
      </div>
    )
  };
};

export default App
