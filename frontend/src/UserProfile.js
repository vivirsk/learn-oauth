import React, { Component } from 'react'
import './App.css'
import useravatar from './icons/user_avatar.svg'
import * as ContactsAPI from './utils/ContactsAPI'
import UserRepo from './UserRepo.js';

class UserProfile extends Component {
  state = {
    repos: [],
    showUserRepos: false,
  };

  componentDidMount() {
    ContactsAPI.getUserRepos()
      .then((repos) => {
        this.setState(() => ({
          repos: repos
        }))
      })
  };

  toggleButton = () => {
    this.setState((oldState) => ({
      showUserRepos: !oldState.showUserRepos,
    }))
  };

  render() {

    const viewReposButton = (
      <div>
        <button onClick={this.toggleButton}>
          {this.state.showUserRepos ? 'Hide' : 'Show'} Repos
        </button>
      </div>
    );

    const viewReposComponent = (
      <ol className='contact-list'>
        {this.state.repos.map((repo, index) => (
          <UserRepo 
            key={repo.id}
            repo={repo}
            show={this.state.showUserRepos}
          />
        ))}
      </ol>
    );

    return (
      <div>
        <h1>Hello ViviRsk</h1>
        <div>
          <img src={useravatar} className="Avatar-logo" alt="logo" />
        </div>
        <div>
          {this.state.repos && this.state.repos.length > 0 ? (viewReposButton) : ('')}
          {this.state.showUserRepos ? (viewReposComponent) : ''}
        </div>
      </div>

    )
  };
};

export default UserProfile;