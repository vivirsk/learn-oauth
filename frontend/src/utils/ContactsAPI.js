const api = 'http://localhost:5000'

let token = localStorage.token

if (!token)
  token = localStorage.token = Math.random().toString(36).substr(-8)

export const getUserAuth = () =>
  fetch(`${api}/`, {
    mode: 'cors',
    credentials: 'include',
  })
    .then(res => res.json())
    .then(data => data["is_authenticated"])

export const getUserRepos = () =>
  fetch(`${api}/?action=repos`, {
    mode: 'cors',
    credentials: 'include',
  })
    .then(res => res.json())
    .then(data => data)
