from flask import Flask, session, request, Response, url_for, redirect, jsonify
import json
import os
import requests
import urllib.parse

app = Flask(__name__)

# Now let's set up a few variables we'll need for the OAuth process. // Fill these out with the values from GitHub
github_client_id = os.environ['GITHUB_CLIENT_ID']
github_client_secret = os.environ['GITHUB_CLIENT_SECRET']
app.secret_key = github_client_secret

# This is the URL we'll send the user to first to get their authorization
# aka Request a user's GitHub identity
authorize_url = 'https://github.com/login/oauth/authorize'

# This is the endpoint we'll request an access token from
token_url = 'https://github.com/login/oauth/access_token'

# This is the GitHub base URL for API requests
api_url_base = 'https://api.github.com/'

# The URL for this script, used as the redirect URL
base_url = 'localhost:5000/next'



def enable_cors(res):
    response = jsonify(res)
    response.headers.add("Access-Control-Allow-Origin", "http://localhost:3000")
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    return response


# Start a session so we have a place to store things between redirects

# First, let's set up the "logged-in" and "logged-out" views.
# This will show a simple message indicating whether the user is logged in or logged out. 
# If there is an access token in the session, the user is already logged in
@app.route("/")
def index():
    """
    Sets up the "logged-in" and "logged-out" views.
    This will show a simple message indicating whether the user is logged in or logged out. 
    If there is an access token in the session, the user is already logged in
    """

    # Start the login process by sending the user to GitHub's authorization page
    if request.args.get('action') == 'login':

        # Generate a random hash and store in the session
        session['state'] = os.urandom(16).hex()

        params = {
            'response_type': 'code',
            'client_id': github_client_id,
            'redirect_uri': 'http://localhost:5000/next',
            'scope': 'user+public_repo',
            'state': session['state']
        }

        # Redirect the user to GitHub's authorization page
        params_map_obj = map(lambda x: '&' + x + '=' + params[x], params)
        url = ''.join(list(params_map_obj))[1:]
        url = authorize_url + '/?' + url

        resp = requests.get(url, headers={'Location': url})  #  --> pip install requests
        return Response(resp)

    if request.args.get('action') == 'repos':
        # Find all repos created by the authenticated user
        params = {
            'sort': 'created',
            'direction': 'desc',
            'affiliation': 'owner'

        }
        headers = {
            'Accept': 'application/vnd.github.v3+json',
            'Authorization': 'Bearer {}'.format(session['access_token'])
        }

        repos = api_request(url=api_url_base + 'user/repos?', params=params, headers=headers)
        repos = json.loads(repos.content.decode("utf-8"))

        return enable_cors(repos)

    if request.args.get('action') == 'logout':
        session['state'] = None
        response = {"is_authenticated": "false"}
        return enable_cors(response)

    if 'access_token' in session:
        response = {"is_authenticated": "true"}
        return enable_cors(response)
    else:
        response = {"is_authenticated": "false"}
        return enable_cors(response)


@app.route("/next", methods=['GET', 'POST'])
def next():
    # When GitHub redirects the user back here, there will be a "code" and 
    # "state" parameter in teh query string
    code = request.args.get('code')
    state = request.args.get('state')

    if code:
        # Verify the state matches our stored state
        if not state or state != session['state']:
            raise Exception("ERROR: Invalid state")

        # Exchange the auth code for an access token
        params = {
            'grant_type': 'authorization_code',
            'client_id': github_client_id,
            'client_secret': github_client_secret,
            'redirect_uri': 'http://localhost:5000/next',
            'code': code,
        }
        github_api_token_response = api_request(url=token_url, params=params)
        access_token = json.loads(github_api_token_response.content.decode("utf-8)"))['access_token']
        
        # Extracted the access token and save it in the session
        session['access_token'] = access_token
        # The next time you visit the page, it recognizes that there's 
        # already an access token and shows the logged-in view

    return redirect(url_for('index'))# "This is the page after redirect"


def clean_access_token_data(token_data):
    """
    The response from GitHub can look like the below, unless the Accept 
    header as json is not specified:
    b'access_token=123456abcd&scope=public_repo%2Cuser&token_type=bearer'
    """
    token_data_str = token_data.decode("utf-8")
    return urllib.parse.parse_qs(token_data_str)


# To make things easier for us, let's define a method, apiRequest() which is a simple wrapper around cURL. 
# This function will include the Accept and User-Agent headers that GitHub's API requires, and automatically decode the JSON response. 
# If we have an access token in the session, it will send the proper OAuth header with the access token as well, 
# in order to make authenticated requests.

def api_request(url, params={}, headers={}):
    """
    This helper function will make API requests to GitHub, setting
    the appropriate headers GitHub expects, and decoding the JSON response
    """
    if request.method == 'POST':
        pass

    auth_headers = {
        'Accept': 'application/json',
        'User-Agent': 'http://localhost:5000'
    }

    if not headers:
        headers = auth_headers

    response = requests.get(url, params=params, headers=headers)  #  --> pip install requests
    return response